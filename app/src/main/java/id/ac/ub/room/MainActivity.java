package id.ac.ub.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.ac.ub.room.recyclernya.adapter.MahasiswaAdapter;
import id.ac.ub.room.recyclernya.model.Mahasiswa;

public class MainActivity extends AppCompatActivity {
    Button btnAdd, btnRead;
    EditText etNama, etNim;
    RecyclerView recyclerView;
    MahasiswaAdapter mahasiswaAdapter;
    private AppDatabase appDb;
    ArrayList<Mahasiswa> mahasiswaArrayList = new ArrayList<>();
    static int[] foto = {R.drawable.ava1, R.drawable.ava2, R.drawable.ava3, R.drawable.ava4};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        btnAdd = findViewById(R.id.btn_add);
        btnRead = findViewById(R.id.btn_read);
        etNama = findViewById(R.id.et_nama);
        etNim = findViewById(R.id.et_nim);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!etNim.getText().toString().equals("") || !etNama.getText().toString().equals("")) {
                    Item item = new Item();
                    item.setNama(etNama.getText().toString());
                    item.setNim(etNim.getText().toString());
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            appDb.itemDao().insertAll(item);
                        }
                    });
                }
            }
        });
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<Item> list = (ArrayList<Item>) appDb.itemDao().getAll();
                        int jmlDataDb = list.size();
                        int jmlDataArr = mahasiswaArrayList.size();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(jmlDataDb != jmlDataArr){
                                    mahasiswaArrayList.removeAll(mahasiswaArrayList);
                                }
                                for (Item item : list) {
                                    Random ran = new Random();
                                    int choose = ran.nextInt(3);
                                    Mahasiswa mahasiswa = new Mahasiswa();
                                    mahasiswa.setNama(item.getNama());
                                    mahasiswa.setNim(item.getNim());
                                    if(jmlDataDb != jmlDataArr){
                                        mahasiswaArrayList.add(mahasiswa);
                                    }
                                }

                                initRecyclerView(mahasiswaArrayList);
                            }
                        });
                    }
                });
            }
        });

    }

    private void initRecyclerView(ArrayList<Mahasiswa> arrayList){
        recyclerView = findViewById(R.id.rv_mahasiswas);
        mahasiswaAdapter = new MahasiswaAdapter(arrayList, this);
        recyclerView.setAdapter(mahasiswaAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}